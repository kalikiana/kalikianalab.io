#!/usr/bin/env python3
from markdown import Markdown
from io import StringIO

from mimic3_tts import Mimic3TextToSpeechSystem, Mimic3Settings
from pydub import AudioSegment

import glob
import logging
import os
import re
import sys


def unmark_element(element, stream=None):
    if stream is None:
        stream = StringIO()
    if element.tag == 'code' and "\n" in element.text:
        # Omit source code snippets
        pass
    elif element.text:
        stream.write(element.text)
    for sub in element:
        unmark_element(sub, stream)
    if element.tail:
        stream.write(element.tail)
    return stream.getvalue()

def strip(text):
    # Drop the front matter first
    content = re.sub('^---[\s\S]+?---', '', text)
    # Strip markdown formatting
    Markdown.output_formats['less'] = unmark_element
    less = Markdown(output_format='less')
    less.stripTopLevelTags = False
    return less.convert(content)

def read(text, audiofile):
    logging.info('Reading {} out loud'.format(audiofile))
    tts = Mimic3TextToSpeechSystem(Mimic3Settings())
    tts.begin_utterance()
    tts.speak_text(text)
    results = tts.end_utterance()
    sound = AudioSegment.empty()
    for result in results:
        sound += AudioSegment(
            data=result.audio_bytes,
            frame_rate=22050,
            sample_width=2,
            channels=1,
        )
    sound.export(audiofile)

def inject(source, audiofile):
    # Inject player where the "more" marker is used
    more = '<!--more-->'
    basename = os.path.basename(audiofile)
    if more not in source:
        logging.info('{} is missing the {} marker'.format(basename.replace('mp3', 'md'), more))
        # Alternatively inject after the front matter
        more = "---\n\n"
    return source.replace(more,
        '''{more}

           {{{{<rawhtml>}}}}
           <audio controls src="../{audiofile}">
           <a href="../{audiofile}">Download audio</a>
           </audio>
           {{{{</rawhtml>}}}}

           \n'''.format(more=more, audiofile=basename), 1)

logging.getLogger().setLevel(logging.INFO)
contentdir = 'content'
filenames = sys.argv[1:] if len(sys.argv) > 1 else glob.glob(contentdir + '/post/*.md')
outdir = 'generated'
for filename in filenames:
    try:
        with open(filename, 'r') as markdown:
            source = markdown.read()
            outfile = filename.replace(contentdir, outdir)
            audiofile = outfile.replace('.md', '.mp3')
            # Generate audio if it's not already there
            if not os.path.exists(audiofile) or os.path.getmtime(filename) > os.path.getmtime(audiofile):
                os.makedirs(os.path.dirname(outfile), exist_ok=True)
                read(strip(source), audiofile)
                with open(outfile, 'w') as injected:
                    injected.write(inject(source, audiofile))
            else:
                logging.info('{} already exists, skipping ahead'.format(audiofile))
    except FileNotFoundError as e:
        sys.exit('Markdown source file {} not found'.format(filename))

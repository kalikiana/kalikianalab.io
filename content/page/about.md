---
title: About Liv
subtitle: Who are they anyway?
comments: false
---

Free software girl, big on tea, fresh fruit and nuts.
Pronouns: [she/they](../../post/2022-03-08-inclusive-language-english-vs-german)

### Looking back

Press some of my (social) buttons to find out more.

[Imprint](../imprint)

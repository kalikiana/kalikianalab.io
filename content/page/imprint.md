---
title: Imprint
subtitle: Who are they anyway?
---

There is no commercial offering or monetizable content here.

### GDPR

This website is statically generated. No user data is being stored or evaluated.

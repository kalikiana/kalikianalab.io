---
title: Smarte Galaxy Watch ist galaktisch
subtitle: Du stirbst nur einmal
date: 2021-12-12T23:13:03+01:00
tags: ["testbericht", "wearable"]
---

Die Galaxy Watch 4 ist für mich ein Upgrade. Benachrichtigungen mit Sonderzeichen, Emoji und Videos auf der Watch anschauen. Ich bin Xiaomi gewohnt - jetzt tausche ich zwei Wochen Akku gegen Apps.

<!--more-->

### Wie smart ist die smarte Watch

Meine Anforderungen sind gering, da die Integration besser sein wird als ich es gewohnt bin. Und als offene Platform wird das noch mehr werden. Wenn die wichtigen Apps auch Wear unterstützen kann das Handy sogar zuhause bleiben. Soweit das traumhafte Potential. Ich denke der Traum hängt nun davon ab, was der Markt haben will. Aber ich greife vor.

### Gehen wir mal auf Tuchfühlung

Der Touchscreen funktioniert wie bei einem modernen Smartphone ohne Navigationstasten. Von links reinwischen um Benachrichtigungen zu sehen, Schnelleinstellungen von oben, Widgets von rechts und Apps von unten. Und die Komplikationen lassen sich direkt auf dem Ziffernblatt aktivieren.

An der virtuellen Lünette zu drehen ist ungewohnt. Mir ist das minimalistische Design des randlosen Displays dennoch sympathisch. Die Haptik finde ich super. Ich merke bei der 40mm-Ausführung kaum das Gewicht am Handgelenk und das Band ist angenehm. Und mir persönlich gefällt das randlose Display ohne Lünette. Schlicht aber sehr hochwertig. Wahrscheinlich hole ich mir noch ein Band zum wechseln, einfach um die Farbe mal abzuwechseln.

Der Vollständigkeit halber sei noch NFC erwähnt. Mit einer Spiderman-Geste kann bezahlt werden, über Samsung Pay oder Google Pay. Und das geht auch ohne Handy und offline.

### Irgendwie kommt mir das bekannt vor

Die Einstellungen über die **Wear**-App sind wie bei einem Samsung-Handy, von Verbindungen über Display bis Entwicklermodus[^1]. Und jenachdem kann man die Komplikationen[^2] auswählen. Und es gibt natürlich Apps, die installiert werden wollen. Von installieren Apps auf dem Handy lassen sich soweit verfügbar direkt die Wear-Versionen nachladen. Bei neuen Apps wird das direkt vorgeschlagen. *Spotify* etwa lässt sich ganz ohne Handy benutzen und Playlists können lokal gespeichert werden. Bei *Komoot* kann man geplante Strecken aufrufen und auf der Watch navigieren. Oder auch *Hue Essentials* um die häusliche Beleuchtung zu steuern.

[^1]: Das übliche Spiel wie bei Android. Auf die Versionsnummer drücken bis eine Bestätigung kommen.
[^2]: Komplikationen sind hier das, was in der analogen Welt extra Zahnräder zur Datumsanzeige wären oder heutzutage der Blutdruck.

Nur hier ein kleiner Haken. Mit den Apps war ich relativ schnell durch und ich vermisse z.B. *PocketCasts* schmerzlich wodurch Podcasts nicht automatisch auf dem Handy landen. So bleibt nur der Umweg über händisch rüberkopierte Folgen wie auf einem klassischen MP3-Player und mein Hörfortschritt wird nicht mehr synchronisiert. Man merkt, dass das umgemodelte WearOS noch nicht die Nutzerzahlen hat, und es sich für viele Entwickler noch nicht lohnt. Das wird sich sicherlich ändern wenn Samsung das Vorrecht verliert und andere Firmen das neue WearOS unterstützen. Zudem nächstes Jahr schon eine aufgemotzte Version davon kommen soll wodurch dann nochmal mehr Leute zuschlagen wenn ihnen hier etwas fehlte.

### Manchmal akkumulieren sich die Probleme einfach

Und der erste Gedanke ist hier der Akku. Direkt am Anfang merkte ich schon, dass der Batteriestand schnell nach unten ging. Auf eine Woche wird sie auch mit Software-Updates nicht kommen. Der Akku ist nicht groß und Apps können auch nur zu einem gewissen Grad optimiert werden. Ein Nachfolger mit einem dicken Akku und sparsameren Prozessor könnte richtig einschlagen.

Immerhin gibt es einige Sachen, die man evtl nicht braucht und ausschalten kann. Ich find es zum Beispiel angenehm, auf das Display zu tippen statt dass das Display durch die Handbewegung angeht. Und ich brauche auch kein AlwaysOn - auch wenn es sehr nett aussieht, ich schaue im Alltag nicht ständig drauf. Es ist nur schade, dass man eigentlich nicht alle Funktionen aktivieren kann. Vielleicht hätte Samsung besser daran getan, die Einstellungen einzuschränken um ein Minimum an Laufzeit zu gewährleisten.

### Manches kann der Laden aber auch einfach

Laden über Qi[^3] ist natürlich top. Endlich kein modellspezifischer Kontaktstöpsel mehr. Ladematten, andere Handys und Powerbanks lassen sich mitbenutzen. Der enthaltene Ladepuk endet in USB-A, was fast schon unerwartet ist. Es stört mich nicht sehr weil ich sowieso beides im Umlauf habe, aber das hätte auch ein austauschbares Kabel sein können. Davon abgesehen gefällt mir der magnetische Halt, sodass die Uhr gut aufliegt. Das ist nicht selbstverständlich.

[^3]: Gesprochen wie die zweite Silbe in Hatschi.

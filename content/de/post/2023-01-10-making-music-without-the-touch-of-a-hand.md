---
title: Schau mal ich kann ohne Hände spielen
subtitle: Du stirbst nur einmal
date: 2023-01-10T21:53:01+02:00
tags: ["testbericht", "theremin"]
images: ["20230107_105024.jpg"]
---

Dieses Instrument welches sich nur durch Bewegungen spielen lässt hatte mich schon lange fasziniert. Leider sind Theremine ursteuer. Dachte ich zumindest bis ich aufs [Open Theremin](https://www.gaudi.ch/OpenTheremin/) kam.

<!--more-->

### Wieviel Gas darf es denn sein?

Vor hundert Jahren arbeitete [Lev Sergeevich Theremin](https://en.wikipedia.org/wiki/Leon_Theremin), auch bekannt als Leon Theremin, an einem Oszillator, der die dielektrische Konstante von Gasen besser bestimmen sollte. Während er versuchte das Prinzip auf andere Einsatzgebiete wie etwa den ersten Bewegungsdetektor anzuwenden fiel Theremin auf, dass sich mit einem angeschlossenen Lautsprecher beim bewegen der Hände Musik ergab. Das war nichtmal ungewöhnlich, schließlich erfand Theremin durch herumprobieren auch [The Thing](https://en.wikipedia.org/wiki/The_Thing_(listening_device)) und einen [mechanischen Fernseher](https://en.wikipedia.org/wiki/Mechanical_television).

### Lässt sich ohne Saiten und Tasten Musik machen?

Eigentlich ging es mir nie um die Physik geschweige denn Abhörtechnik. Ich befand mich auf einem Konzert ohne zu ahnen was mich erwartete. Dort gabe es eine Person, die einfach die Händer durch die Luft bewegte und damit Töne erzeugte. Einfach magisch. Ich hatte keinen Schimmer wie das geht - falls dir das Theremin neu ist verstehst du mich vielleicht gut. Die Antennen sind natürlich nicht unsichtbar aber selbsterklärend sind sie nicht. Meine musische Bildung hatte Streicher, Flöten und Klaviere behinhaltet. Tasten und Löcher sind relativ einfach zu bedienen. Saiten vibrieren. Trommeln trommeln. Aber wie soll das gehen wenn du es garnicht anfast?

### Kommen wir zum Open Theremin

Es gibt da einen Schweizer Ingenieur names Urs Gaudenz, der ein Festival für kreative Anwendung von Technik besuchte. Gaudenz war am Theremin interessiert, fand aber, dass die üblichen Bausätze mehr schlecht als recht zum ernsthaften Musizieren taugten. Wieso sollte sich kein vollwertiges Theremin bauen lassen? Die ursprüngliche Version war ein Shield für Arduino (zum aufstecken). Wenn du was mit Mikrocontrollern machst sagt dir das bestimmt was. Vor dem Raspberry Pi führte eigentlich kein Weg daran vorbei.

In der aktuellen Version 4 bekommst du das Open Theremin sogar fertig gelötet und gestimmt, wodurch durch es ohne technische Fertigkeiten bespielen kannst. Gleichzeitig sind das Platinenlayout und die Software komplett offen. Für jemand wie mich dier gern Code anfasst aber mit dem Lötenkolben mehr Schaden anrichtet als xier zusammenlöten kann ist das schon ansprechend.

### Was ist dabei?

Ich hab mir das **Deluxe Bundle** geholt, da es schon fertig zusammengebaut ist inklusive eines 3D-gedruckten Covers, Stand und USB Type C-Kabel. Dazu kommen die Aluminiumantennen. Für weniger als 200€ vor Steuern[^1] fand ich das fair und es geht noch günstiger wenn du es auch zusammenbauen kannst. Profitheremine fangen bei 500€ an und kosten gerne vierstellig.

[^1]: 50€ gingen noch an das Schweizer Zollamt.

Der Aufbau beginnt mit dem anschrauben der Antennen. Die gebogene Antenne mit der die Lautstärke geregelt wird und die hohe, aus zwei Telen bestehende Antenne für die Tonhöhe. Das Gewinde passt auch auf andere Stative falls du schon eines hast. Strom kommt über USB Typ C. Wichtig ist nur, dass der Netzstecker Erde hat oder das Theremin über einen zusätzlichen Draht geerdet ist[^2], ansonsten wird hauptsächlich laut und nicht magisch. Schließlich misst dieses Instrument seine Umgebung um zu funktionieren. Und du brauchst einen aktiven Lautsprecher - in meinem Fall ein Brüllwürfel mit Klinkeleingang.

[^2]: Du kannst den Draht zur Not auch an dir selbst erden.

![Vorderseite der Schachtel mit der Aufschrift OpenTheremin V4 by GaudiLabs](./20230105_141541.jpg)
![Geöffnete Schachtel mit sichtbarer Anleitung mit Einleitung und Abbildung des aufgebauten Instruments](./20230105_141646.jpg)
![Blick auf das verpackte USB-Kabel und das Stativ](./20230105_141728.jpg)
![Frontansicht des Theremins mit Reglern für Volume, Pitch, Register und Timbre, Function und Ground unten links und In/Out, USB-C, Audio und CV unten rechts](./20230105_141844.jpg)
![Nahansicht der rechten Seite des Theremins mit USB und zwei Klinkensteckern](./20230105_141859.jpg)
![Nahansicht des USB-Kabels mit Daumen](./20230105_142130.jpg)

## Performance

Klar ist das kein Instrument zum sofort losdaddeln. Ein absolutes oder relatives Gerhör empfielt sich, aber du musst dich auf das Instrument einstellen, mit anderen Thereminen nimmt es sich in der Hinsicht nichts. Jede Position um die Tonantenne entspricht einem Mikroton. Anders gesagt wie bei einer Geige gibt es keine absoluten Töne und die Oktave hängt von der Größe deiner Hand ab. Jeda Thereminist*in spielt ein wenig anders.

Das Kalibrieren ist zumindest einfach. Die Taste 3 Sekunden gedrückt halten und eine Minute warten, das ist es schon. Dann lässt sich über **Volume** and **Pitch** noch die Empfindlichkeit der jeweiligen Antenne anpassen, genau wie z.B. ein Theremin von Moog. **Register** passt das Tonspektrum an. **Timbre** verändert die Kurve (im physikalischen Sinne).

![Katze pirscht sich von hinten an um das Theremin näher zu inspizieren](./20230107_105024.jpg)

### Mehr als ein Theremin

Spezialeffekte sind nicht eingebaut, was auch normal ist. Allerdings ist es ja freie Software unter der GPL and [es gibt eine MIDI-Implementierung](https://www.gaudi.ch/OpenTheremin/index.php/opentheremin-v4/midi-interface)! Zumindest aktuell wird das nicht ab Werk unterstützt, heißt dafür musst du es selbst flashen. Falls dir MIDI nichts sagt denkst du vielleicht an billige eingebaute Lautsprecher im PC. MIDI ist das was mit dem Synthesizer spricht um diverse Instrumente und Klänge produzieren zu können. Wenn du schon immer Harfe oder Saxofon spielen wolltest und die Instrumente dir nicht lagen, so geht's![^3]

[^3]: Etwas lässt sich sogar cheaten indem du änderst wie das Theremin Bewegungen verarbeitet.

### Portabilität

Das Open Theremin ist schon sehr mobil und leicht gebaut, passt in kleine Taschen und läuft auch über eine Powerbank, vorausgesetzt es ist irgendwie geerdet. Als Minimalist*in gefällt mir das sehr und ich möchte es demnächst auch draußen aufbauen. Im Park, Hackspace oder auf einer Konferenz geht das problemlos.

### Mein Urteil

Würde ich es wieder kaufen? Ohne zu zögern. Es ist easy aufzubauen, extrem mobil und anpassbar. [Konzerte geb ich aber wohl ersma nicht](https://www.youtube.com/watch?v=_bh-KA22DyM).

![Auf einem Tisch aufgestelltes Theremin mit Antennen und Stativ, das an einen Lautsprecher angeschlossen ist. Dahinter die obligatorische schlummernde Katze](./20230107_125311.jpg)

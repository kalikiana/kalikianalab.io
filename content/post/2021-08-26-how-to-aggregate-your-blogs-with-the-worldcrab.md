---
title: How to aggregate your blogs with the world crab
subtitle: You only live once
date: 2021-08-26T11:15:12+02:00
tags: ["rust", "howto", "hugo"]
---

After [dipping my toes into Rust](../2021-03-26-dipping-my-toes-into-rust) and [preparing the boilerplate](../2021-05-07-setting-up-a-new-rust-project) I'm going to explain what my first project is and how to use it!

<!--more-->

### What itch am I scratching here anyway?

I've been doing a fair bit of [blogging with Hugo](../2021-02-12-setup-gitlab-pages-blog-with-hugo) recently and encouraged others to pick up the habit. But available *planets* based on RSS or ATOM feeds seem to be missing support for tags and categories. In some configurations they are part of the feed, but they're also typically ignored. Other metadata is specified separately in the respective planet's configuration. So I figured I'd try and see if I could come up with an aggregator that could handle those aspects. I couldn't confirm if somebody tried this before and failed, but I might as well give it a go and I'm sure I'll learn something in the process!

### The world is a disc riding atop a massive space crab

Setting up your *planet* is straightforward. No need to duplicate any existing content here, but what we want is a list of URLs that function as sources, which is configured in the file `disc.yaml` at the top of the project folder:

```yaml
blogs:
- https://gitlab.com/kalikiana/kalikiana.gitlab.io.git
- https://openqa-bites.github.io/index.xml
```

If you were to inspect those URLs you would find that they are *git* repos and *RSS* feeds respectively. Supported source types are detected automatically. If it's a git repo it is assumed to contain posts written in markdown, using front matter as used by static site generators like **Hugo** or **Jekyll**.

### How do I install this?

The [world crab](https://github.com/kalikiana/worldcrab) can be installed several ways. Pick whatever is most convenient for you:

```bash
git clone git@github.com:kalikiana/worldcrab.git
cd worldcrab
cargo install --path .
```

Installing from a local copy is most convenient if you plan to contribute or keep up with updates on a regular basis:

```bash
cargo install --git https://github.com/kalikiana/worldcrab
```

### Populating the disc with Hugo

The world crab takes care of the content but not generating the website. We'll use [Hugo](https://github.com/gohugoio/hugo) in the following example, although [Jekyll](https://jekyllrb.com) or [Sculpin](https://sculpin.io) will work as well if you're already familiar with those:


```bash
hugo new site -f yaml disc
git submodule add https://github.com/halogenica/beautifulhugo.git disc/themes/beautifulhugo
sed -i s@.Permalink@.Params.original_link@ disc/themes/beautifulhugo/layouts/partials/*.html
```

What this does is prepare a new website with the *Beautiful Hugo* theme and support for `original_link` which is needed to link back to the original sources.

```bash
wordcrab disc
hugo -s disc -t beautifulhugo
```

The world crab will pull in the sources and cache them in `.blogs` before populating `disc/content/post` with content. Finally *Hugo* to generates everything. You can choose to further customize your setup from here, or treat all of these as generated files.

### Refresh and publish changes automatically

One of the easiest ways to build and make this is via [GitHub Actions](https://github.com/features/actions) assuming the project is hosted on GitHub. Create a file `.github/workflows/disc.yml` with the following contents:

```yaml
name: Update disc

on: [push]

jobs:
  build:
    runs-on: ubuntu-latest

    steps:
      - uses: actions/checkout@v2
      - name: Update blog contents
        run: worldcrab disc
      - name: Refresh the disc
        run: hugo -s disc -t beautifulhugo
```

### Where is the space crab headed?

Since this started as a learning opportunity first of all there's no long list of planned features. It's conceivable that other source types will be added and no performance analysis has been done so far. This will depend on wether this is going to be used in production.

If you want to use this project, feel free to reach out and [file an issue](https://github.com/kalikiana/worldcrab/issues) and tell your story!

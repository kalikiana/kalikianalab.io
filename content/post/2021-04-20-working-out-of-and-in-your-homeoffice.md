---
title: Working out of and in your homeoffice
subtitle: You only live once
date: 2021-04-20T15:26:40+01:00
tags: ["semi-technical", "homeoffice", "health"]
---

### Homeoffice is awesome for so many reasons

I've worked remotely for more than a decade. Actually all of my software development career has been one where going to the office was the exception. Something I do occasionally to meet some of my colleagues. Actually when I see my colleagues it's more typically at software conferences rather than company headquearters.

Benefits that come with homeoffice are numerous:

- On paper I've got a 9-5 job
- I don't need to move to do my job[^1]
- I can adjust to the demands of my personal life
- I can adjust to make my colleague's life easier

Drawbacks that come with homeoffice are numerous:

- I don't need to move to do my job[^1]

You can see where I'm going with this. I want to talk about physical health. Imagine you are so flexible with how and where you work from, it also becomes very easy to get so comfortable that your biggest commute on most days is from the chair to the kitchen and to the bathroom.

[^1]: Neither to a new city nor within the house. Pun intended.

### Why is my comfy chair a pain in the neck?

Maybe you've got a super comfortable chair. Nice lighting perfectly tweaked to your personal preference. The most amazing desk. Lots of beautiful plants around you. But somehow you've still got neckpain or an aching back.

This is going to be slightly controversial but I don't think there's a chair in the world that you can sit on for many hours every day and still be healthy. You might be able to find one that is extremely comfortable but it's not going to stop you developing bad posture and back problems long-term. People who work at a desk several hours a day will eventually look into seeing a physiotherapist. Those who work remotely are even worse off because they're more likely to work overtime since there's no commute and the office is open 24/7.

Some studies actually suggest sitting for many hours greatly compromises your metabolism and your cardiovascular system. I recommend you do your own research rather than take it from me, though. In this article I'm going to focus on how to be more active during the day, rather than why and let you decide for yourself if it's making you more comfortable.

### So what do I do instead of sitting on my chair all day long?

There's many ways you can be more active. Either by changing how you work or what you do outside of work. Most of them involve not sitting at all.

- Alternate where and how you sit
- You can work in a standing or squatted position
- Take proper breaks away from your workspace
- Walk around when you're on a call
- Do some bodyweight exercises

### May the real slim shady please stand up?

Stand up instead of sitting. While standing your body needs to actively keep up your back which requires significantly more energy. So you burn more calories and exercise your back musculature even if you stay in the same spot. All you need to do is work from an elevated surface. This could be a kitchen countertop of a suitable height, a shelf or an adjustable desk. Have a look around your house and see what options are available! If you don't have anything that's agreeable stacking cardboard boxes is an easy hack, even to initially test it out.

You're pretty much guaranteed to find working while standing awkward and exhausting the first few times you try it. So be sure to ease into it slowly. Try a half hour and see how that feels. Maybe an hour the next day. Stick to that for a week. Consider increasing the time you stand while doing your regular work.

As you get used to standing more you should notice that it can feel pretty good. Some part of you will like it a lot and you may have more energy at the end of the day than when you're sitting. I will even go so far as to say when I work a full day standing I'm going to be so energized I'm itching to exercise and go for a run contrary to what you might expect. Bear in mind of course I've been doing this for a few years now.

### Sit down somewhere else for a bit

This option is naturally the easiest when you work from a laptop that you can move freely around the house. It's also probably the easiest to try out since you don't need anything else. Grab your machine and pick somewhere nice to sit. It doesn't matter if it's a comfy sofa, a garden chair or at the dining table. Even your bed is allowed. And to be clear I'm not saying to abandon your trusted and proven desk, I'm saying alternate where you sit. Spend a couple hours on the sofa or whichever you decide to try out. Let your body adjust to it. And that's actually the whole point here. Get away from that fixed posture that you're used to sitting in and allow your muscles to adjust to it.

### Squat or kneel in front of your desk

Many people in what's considered the Western hemisphere aren't used to squatting or stop doing it as they get older. I'm not going to go into detail here, but consider if a squatted or kneeling position is something you can be comfortable in for some of your working hours. What I like to do personally is squat during meetings and only change my position after the call.

### Move around a little and take proper breaks

Use breaks not just to make coffee but to get a bit of fresh air if you can. Look out the window or stand on the balcony for a bit. Play with your furry room mate if you have one. Even if you don't have that much time it's good to get away from the working environment a few minutes. Make a point of not looking at technology during your breaks.

### Run before or after working hours

A straightforward option is to go for a run before or after work. This is usually the easiest because you can plan on being up early or make time for a run in the evening. It doesn't matter so much how long or how fast. What matters first of all is that you do it. And that you do it on a regular basis, such as every other day or once a week. And long-term you can test out your personal limits.

Runs during lunch breaks are totally an option, and I actually like to run instead of having lunch which works very well with intermittent fasting. But I don't recommend doing both in the same break. 😜️

### Walk around during a call or while your code compiles

Any activity where you're not actively typing, browsing or reading on a screen can be an opportunity to get moving! If you're on a call and talking to a colleague you could use the opportunity to move around a little. Focus on the conversation rather than what's on the screen. Obviously this only works when you're not required to do a lot of reading or something needs to be presented during the call. Similarly if you're working with code and waiting for code to compile or for a test to finish. If it's going to finish soon but takes at least long enough to make a coffee, you could equally use the time to walk around or put in a light exercise - concrete ideas to follow in the next sections!

### Put that weight of yours to good use

And what you can also do very easily from the comfort of your own home is bodyweight exercises. No specialized equipment is needed and you can do them even without access to a gym. Read on for some ideas!

Regardless of how heavy or slender you are, you can use gravity to your advantage. The beautiful reason is that if you don't have a lot of muscle it'll be difficult to lift yourself off the ground or up into the air. Conversely the more muscle you have the heavier you are because muscles are denser than fat. There's no scenario where this no longer works! 💪️

### Pull me up, pull me down

For starters you can do **push-ups**, or in other words use your hands and feet to get yourself off the ground. Get down on all fours, lower yourself to the ground until your chest or stomach gets very close to the ground and pretend there's a million needles under you that you really don't want to touch because it's going to hurt like hell. Once you're in that position *push* yourself up in such a way that you steadily move upwards. If that's strenuous and you're tempted to shift yourself a little to the side to make it easier, try not to. You want to use all of your muscles here rather than your brain. Focus on the movement. Last but not least, aim for 10 quick repetitions. If you can do more you're probably taking a shortcut. If you can't do as many, you're allowed to go easy on yourself.

The second obvious option is **pull-ups**. Pretty much the opposite[^2], you use your muscles to *pull* your own body up and away from the ground. To be sure you'll need a sturdy door or table to do this. If you're lucky enough to have a bar or access to the underside of a stairway those works well, too. Just be sure it's meant to sustain a bit of weight. I've tried a lot in different places. Sometimes you can find exercise options on playgrounds or in parks.

[^2]: Well, unless you're a bat you won't exactly be hanging off the ceiling. But do tell me how it goes if you're trying that out.

### Lift some weights and carry that burden

In addition to your own body I'm sure you'll find something that needs to or can be lifted and carried around. And it doesn't need to be professional equipment. Look for something that's moderately heavy. If it's annoying to carry from one room to another it's probably suited really well for this!

In some countries like the US and Spain you get big jugs of 5, 8 or 10 litres of juice or water which can serve as weights of the according measure. Alternatively you can get a big bucket and fill it up[^3]. I'm really suggesting basic household items. Be a little creative. If it's heavy and you can lift it you can probably use it for your workout.

If you're getting the hang of it you can start looking for unwieldy items to prevent yourself from getting used to the exercise and making it too easy. One of my personal favorites is my foldable bike 🚲️ because it's around 15kg and its particular shape makes it difficult to hold regardless of how I grab it.

[^3]: Or a chamber pot. But be rather careful. You don't want to spill anything.

### How long until I get that beach body?

I have no idea and that's not why I do it anyway. My body is telling me that something's wrong by being in pain. And I find that I get a lot of energy and sleep better when I work out on a semi-regular basis. I don't follow a super strict regimen because honestly when it gets too repetitive and feels like a churn it's in my nature to stop doing it. Variation is what keeps me going. And I'm sure a long-term side effect is that I'm leaner and stronger. I don't do it for the beach or for anyone else, though. I just want to be healthy.

Either way I hope I was able to inspire you a little bit if you read this far. Let me know how you like it and we can compare notes. I'm always looking to try new exercises to keep things fun!

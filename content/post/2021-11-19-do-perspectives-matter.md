---
title: Do perspectives matter
subtitle: You only live once
date: 2021-11-19T14:41:12+01:00
tags: ["semi-technical"]
---

When you live in the desert with barely food or water, you must be patient? Or are you desperate to try strange foods? And what about the curiosity? 

<!--more-->

### Let's play a game of context

I'm going to use a fictional culture as a frame of reference, explaining succinctly and the way I would a real culture that maybe you heard about before but not really. There's no spoilers here if that matters. No concepts mentioned are actually explained in terms of the books, they are merely discussed from the perspective of someone who heard about it. If you're quite familiar with some of this you might wonder if you misunderstood what you read.

### What's in a body's water

The Fremen on the planet Dune wear suits that trap moisture because they live in the desert with a roasting hot sun and ice-cold nights, away from the few visitors from other planets. It's common to be stabbed or get eaten by a big worm. Whenever possible **the person's water will be reclaimed** because it belongs to the tribe.

- What do you think they probably do with their dead?

The first time I read about Fremen I knew what their planet was like. It's so hot that you sweat a lot and wouldn't survive long unprepared. Water is important but also scarce. And there's little mention of animals or plants in the desert where they live. Maybe the author, Frank Herbert, really didn't care about this particular detail because it's self-evident? He never saw the need to explain that there's no agriculture because there couldn't be. Most of the planet can't be desert and farmland at the same time.

### What do they do with the bodies?

So maybe by water the Fremen really mean flesh here? The body of a person is taken away and prepared. Presumably to a room where fresh meat is carved? Some of it dried to preserve longer. Of course if they're traveling too far they might improvise with knives.

Alternatively water here means blood. Some desert peoples like the Massai harvest the blood of their cattle in addition to their milk. It might bridge the gap from the lack of animals.

Water could also literally mean water. A human contains a lot of water and loses it throughout the day. While you can debate how long a person can go without food or water, it needs to come from somewhere. Necessary equipment might make it infeasible out in the desert, though?

It's also possible that the Fremen buy food from another planet with whatever they obtain from the bodies. Or they simply use them as organ donors? Or trade the organs on other planets?

### Do perspectives matter

Frank Herbert eventually explains what the Fremen do. What I wanted to demonstrate, though, is looking at the same premise in a number of ways:

- Did you write down your own interpretation earlier?
- Do you re-consider after going through my thoughts?
- What did you find most awkward discussing this?




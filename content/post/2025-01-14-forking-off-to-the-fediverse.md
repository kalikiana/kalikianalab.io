---
title: Forking off to the fediverse
subtitle: You only live once
date: 2025-01-14T19:01:23+01:00
tags: ["howto"]
---

I've written about [making using without touching the instrument](../2023-01-10-making-music-without-the-touch-of-a-hand) before. This time we're making music by programming it.

<!--more-->

### What the mastodon is this fediverse anyway?

Ever use email? **Email** is federated. You pick an email address with a domain. The domain depends on the **provider** which is what's called an **instance** or a **server** in the fediverse. All of these words conceptionally mean the same thing.
**ActivityPub** is a newer technology underlying the fediverse. Think of it like mixing and matching different types of services like tools in your toolkit or toys in your toy box. And you can use all of them and have your friends play with you!

`liv@stim.toys` is my email. I have a user name and a domain. Regardless of what *provider* and **client** you are using to send emails, they will find their way to me. Your email might be free, sponsored by ads or paid for with real currency. That's what federation is all about.

Federation also means there is a ton of instances out there. How would you find them, though, you ask? It is easy:

Check out [some of the Mastodon servers out there](https://joinmastodon.org/servers). While the choice may be overwhelming at first it's worth picking one that vibes with you.

### I'm a roman. I like forums

[Lemmy](https://join-lemmy.org/) might be for you if you strive in communities that discuss and rant about the latest toga fashion advice, news and other hot topics.

*Lemmy* speaks **ActivityPub** meaning it is compatible with *Mastodon* an

### Can I haz pictures, too?

I was originally going to recommend *Pixelfed* here. Unfortunately the creator has a history of posting hateful comments and pretending it never happened. So for now I have no rec and still need to find something better.

### Okay what about videos then?

[PeerTube](https://joinpeertube.org) is videos without the commercial incentive to push for that appeal to the masses and potentially exploit creators.

### Which pill should I go for?

The matrix is also federated. By which I mean the **protocol** 😼 Although a popular default is **Element** which is both the name of a public **Matrix** instance and a client available for many platforms, it is worth considering what's out there. Which is not to say *Element* is not a great default.

Now say you want to be in touch with people and make it easy for them. Sharing your user name is not that handy when someone may not have a client yet. Luckily it's easy to share a universal link here:

https://matrix.to/#/@liv:stim.toys

In this example my handle is `@liv:stim.toys`. First part is my username. Second part is my server name. You can also type this in your client of choice.

### How about running my own instance then?

[GoToSocial](https://gotosocial.org/) is a lightweight option for one person or fewer users with low resource requirements. Despite that it's feature-complete and themable. A container image which can be used with *podman* or another container runtime is provided, too.
Note that *GoToSocial* comes without a built-in client by design.

Depending on your budget [you might want to setup a free server](https://josh.is-cool.dev/running-a-mastodon-instance-entirely-free-forever/). Keep in mind this definitely requires knowledge on setting up a domain, a sever and connecting the pieces yourself.

[There are real pitfalls in spite of the benefits](https://jvns.ca/blog/2023/08/11/some-notes-on-mastodon/) so consider that you will lose features like a **local timeline** and lovely people to **take care of moderation**. This might not even be that much of a concern and some of this is just technical details.
Obviously I'm not saying this to detract you from doing it since I chose to go that route myself. This isn't so much of a concern if you're into hash tags like me you typically prefer to carefully pick your followers. Or if you plan to share the instance with other people.

### Hosting might be a mammoth of an undertaking

If maintaining an **ActivityPub** server is not your thing, and perhaps you want **Matrix** and other services, too, there is another option. There are awesome services that offer hosting or maintenance of your setup for you.

[etke.cc](https://etke.cc/) offers hosting or maintaining Matrix as well as other federated services for you. They can setup SSO, audio, video and social services as well as email, calendars, a VPN, and even password management all based on open source software plus a bunch more. This makes the whole thing a more complete package.
Not to forget **bridges** to other services which don't natively speak *ActivityPub*. [Bridges](https://etke.cc/help/bridges/) are a great way to make *Matrix* the one tool to combine them all. You can have web hooks and bots, too!
Note that this will cost a couple more bucks.
Custom domains are also supported.

[communick](https://communick.com/) is another option focussed on Matrix, Mastodon and audio services. It is also quite affordable and takes privacy seriously including payment options.

### Closing words

I didn't mention all of the platforms and hosting options. This is an excerpt for those who are totally new to the game. Too much choice tends to stop people knowing what to choose and that would defeat the purpose of getting you started on your journey into the fediverse and perhaps leaving behind your walled gardens, or at least having a first look what's beyond that wall.

It would make me very happy to have tempted you. Naturally you also know how to get in touch with me, so I welcome you to say Hi. Whether you came from fedi and found something new or started out completely from square one!

---
title: Looking after your flatpaks
subtitle: You only live once
date: 2021-09-18T14:18:46+02:00
tags: ["howto", "opensuse", "flatpak", "microos"]
---

Do you have many flatpaks? From various remotes added over time? Several themes installed? Disk space is starting to run out? Read on for some tips!

<!--more-->

### Living the flatpak life

My main development machine these days runs [MicroOS](https://get.opensuse.org/microos/). My default used to be **Tumbleweed** and this was one of several options in my boot loader. When MicroOS desktop was very new it had certain problems, I had to tweak it a lot and add packages for apps I needed and I don't like to compromise my work for the sake of living on the edge. At this point all apps that I need are available as flatpak versions - granted I use *flathub-beta*[^1], and you may find something missing if you don't use it. Of course you can still run *Tumbleweed* or *Leap* and simply use flatpaks and packages side by side.

[^1]: `flatpak remote-add --user flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo`

### Appearances do matter

Some people are very happy with the default appearance. Personally I need a dark theme because it calms me down, and too much visual noise distracts me. It helps to know that themes for **Gtk3** apps use a common prefix and are easy to find that way:

    flatpak remote-ls flathub | grep org.gtk.Gtk3theme
    Greybird-dark GTK+ Theme        org.gtk.Gtk3theme.Greybird-dark         3.22
    Materia-dark-compact GTK theme  org.gtk.Gtk3theme.Materia-dark-compact          3.22
    Yaru-Aubergine-dark GTK theme   org.gtk.Gtk3theme.Yaru-Aubergine-dark   21.04   stable

There's a lot of themes, so I'm not going to try and recommend one. Although it's handy to know how to apply the theme via the command-line in case your desktop environment doesn't allow you to choose it:

    set org.gnome.desktop.interface gtk-theme Greybird-dark
    set org.gnome.desktop.interface gtk-theme Yaru-Aubergine-dark
    set org.gnome.desktop.interface gtk-theme Materia-dark-compact

There's one catch with this and probably my one remaining gripe with flatpaks overall. The desktop environment won't be able to use the same theme out of the box. It's not installed from a flatpak and it may not even be compatible. This is a big plus in terms of reliability as well as a major annoyance because now I need to install the same theme system-wide from a package or link it into the `~/.themes` folder[^2]. Again, no recommendation from me here.

[^2]: More precisely link `~/.local/share/flatpak/runtime/org.gtk.Gtk3theme.*/*/stable/active/files` to `gtk-3.0` and place it under a subfolder named like the same.

### Running out of space?

Do you ever wonder if you have too many flatpaks taking up diskspace? Or maybe you get a warning about free disk space and this folder is at the top of your list in the **Disk Usage Analyzer**? Here's how you can find out how much space they take up:

    du -sh ~/.local/share/flatpak
    21G ~/.local/share/flatpak 

**Note**: If you have flatpaks installed system-wide they live in `/var/lib/flatpak`. This is not relevant in my case, but the following advice applies the same.

    flatpak uninstall --unused
    18G ~/.local/share/flatpak

If you have pinned runtimes you'll get a message like this:

    These runtimes in installation 'user' are pinned and won't be removed; see flatpak-pin(1):
      runtime/org.freedesktop.Platform/x86_64/21.08

What this means is that you explicitly installed the runtime, or you pinned it via `flatpak pin`. If that was a mistake or you no longer need it, you can easily rectify that:

    flatpak pin --user --remove
    Pinned patterns:
      runtime/org.freedesktop.Platform/x86_64/21.08
    flatpak pin --user --remove runtime/org.freedesktop.Platform/x86_64/21.08

Afterwards it'll be treated the same and removed if no app needs it. In my case the runtime is still being used so there's nothing more to remove:

    flatpak uninstall --unused
    Nothing unused to uninstall

### Too many remotes?

And here I'm not talking about a situation in the living room, but rather **remotes** as in where your apps and runtimes come from. This is what repositories, archives or stores are called in other contexts. Typically you don't want to download individual apps from files but use `flatpak install` or **GNOME Software** and just type the name of what you're looking for. Most importantly this way you don't have to worry about where to get updates from.

I can't remember exactly what remotes I added in the past. Luckily it's easy to check:

    flatpak remotes
    Name          Options
    flathub       user
    flathub-beta  user
    gnome-nightly user
    kdeapps       user
    pingus        user,no-gpg-verify

Okay so I have a bunch of remotes here. Still not sure why I have those. If only I could check what's provided by a particular remote:

    flatpak remote-ls gnome-nightly

If I decide I no longer want a particular remote I can also delete it easily:

    flatpak remote-delete kdeapps

**Note**: If you didn't do your due diligence and something still uses this remote, flatpak will prompt you to get rid of it before deleting the remote.

If you have a remote that provides alternate versions of an app also available elsewhere, that's not a problem. For instance having both `flathub` and `flathub-beta` is safe. Whenever you try and install something available in multiple remotes, you will be promted to disambiguate. And when you `flatpak search` it will also tell you what remote or remotes an app is provided by.

### Further considerations

- There's the case of [managing GNOME Shell extensions through the browser](https://gitlab.gnome.org/GNOME/chrome-gnome-shell/-/issues/29). Browsing and downloading isn't a problem, but you can't install them directly. And [Extensions](https://flathub.org/apps/details/org.gnome.Extensions) can update, but not install them. 🤷‍♀️️
- Commands [in toolbox containers](../2021-02-22-developing-in-toolbox-containers) can't access flatpaks easily. `flatpak-xdg-open` is awesome. Access to abitrary flatpaks would be even better!

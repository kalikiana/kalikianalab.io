---
title: Inclusive language - English vs German
subtitle: You only live once
date: 2022-03-08T21:04:45+01:00
tags: ["semi-technical", "inclusiveness", "diversity", "gender", "terminology"]
---

They? Them? xier? dier? Why is gender neutral German so difficult and how do you make it work?

<!--more-->

### What are your pronouns?

An innocent question which can make you feel very welcome or lead to an icecold surprise. I think many people simply don't know what this is about. Let's start from the beginning, though:

Case in point, say you go by **they/ them**. What's that mean, though?

Cary likes cats. **They** like cats. Tell **them** about your cat. **Their** cat is called Pyewacket.

The singular they has been around since the 14th century. Shakespeare was no stranger to it. Plural they isn't even much older. Although the generic he is being defended as a gender neutral term to this day.

### Was sind deine Pronomen?

In German this isn't quite so clear-cut. Historically the language has never known ambiguous pronouns. That is, until very recently:

Kim mag Katzen. **Xier** mag Katzen. Erzähl **xiem** von deiner Katze. **Xiese** Katze heißt Aladin.

Let's take a closer look at the cases here:

|Frage|Pronomen|Beispiel
|-|-|-|
|Wer?|xier|Xier mag Katzen|
|Wessen?|xieser|Das Spielzeug gehört xieser Katze|
|Wem?|xiem|Erzähl xiem von deiner Katze|
|Wen?|xien|Frag xien etwas über Aladin|
|Welche?|xiese|Streichle mal xiese Katze|

The agreement with the object remains as usual, according to the grammatical gender:

xiese Katze
xiesen Katzmann
xieses Kätzchen

What if, you ask, the gender of the person themself is ambiguous? Why thank you, there is a solution for that as well:

|Frage|Beispiel|
|-|-|
|Wer?|Das ist xiesa Freund*in.|
|Wessen?|Das ist das Kätzchen xiesas Freund*in.|
|Wem?|Das Kätzchen gehört xiesam Freund*in.|
|Wen?|Das Kätzchen mag xiesan Freund*in.|

And not to forget German has gendered articles:

|Frage|bestimmt|unbestimmt|
|-|-|-|
|Wer?|dier Freund*in|eina Freund*in|
|Wessen?|dies Freund*in|einas Freund*in|
|Wem?|diem Freund*in|einam Freund*in|
|Wen?|dien Freund*in|einan Freund*in|

### Lose the grammatical gender

You may have noticed my use of **Freund*in** in the examples. Many words in German require gender to be specified. In other cases the grammatical gender is oblivious to the gender of the individual, or there is no established noun.

|maskulin|feminin|unspezifisch|
|-|-|-|
Freund|Freundin|Freund*in|
Bruder|Schwester|Geschwister|
Vater|Mutter|Elter|
-|-|Impfling|
Bester Freund|Beste Freundin|Lieblingsmensch|
Arzt|Ärztin|-|

### The variations

Like in English, there's other pronouns or variants with some differences. I'm not going into detail because my motto for this blog is, feel free to take my example and run with it. This is a pragmatic outlook, not an overview of everything out there.

---
title: How to do 4k on Linux in 22
subtitle: You only live once
date: 2022-04-19T16:12:24+01:00
tags: ["howto", "gnome", "hidpi", "4k"]
---

High resolution displays are great. Not just to count pixels but because you get real good font rendering and graphics without aliasing. Sadly it's still something requires a bit of tinkering.

<!--more-->

### Scaling is great

If you want 4k you may want scaling. Ultimately what sold me is fonts. Pixelated approximation aka anti-aliasing is straining for human eyes[^1] and CJK scripts are hard to read at lower resolutions. Most icons and avatars look blurry if you're using a web browser or a chat app. I even use 125% on a 1080p screen because it improves the rendering and still results in a comfortable user experience.

[^1]: Possibly also cat eyes but they seem to lose interest faster than they can get a headache.

### Ramp up the scaling factor in GNOME

The good news is that this is finally a setting for every monitor if you're using more than one. For a 3840x2160 screen for example you may want 200%. Simple. Done.

Wait a minute, what about other resolutions like 2560x1440? Or that 1920x1080 screen that I mentioned before? Let this be the first of many experimental options you'll be learning about here:

    gsettings set org.gnome.mutter experimental-features "['scale-monitor-framebuffer']"

After this is set you need to restart. After that more scaling options will appear in the monitor settings!

### Firefox and Thunderbird look ugly

In particular the Wayland support for Mozilla apps is still experimental and hence not enabled out of the box. As a result you get scaled up fonts and icons. To use it you need an environment variable that you could simply add to your `~/.bashrc` or whatever shell init file:

    export MOZ_ENABLE_WAYLAND=1

Don't forget this will only apply in a freshly logged in shell.

### Proper scaling in Chrome and Chromium

Both of the browsers will seem to work but you still get upscaled fonts and graphics. That's because the support is considered experimental (deja-vu, eh?).

There's a file called `$XDG_CONFIG_HOME/chrome-flags.conf` or `chromium-flags.conf` respectively. If you're [installing everything from flatpaks](../2021-09-18-looking-after-your-flatpaks) like me this probably translates to something like `~/.var/app/com.google.Chrome/config/chrome-flags.conf`. Otherwise you'll find the equivalent files in `~/.config` or wherever your configuration places these files.

You can also add these flags directly on the command-line, or to a `.desktop` file in `~/.local/share/applications`. Personally I prefer config files which I can sync without overriding more than necessary.

```sh
--enable-features=UseOzonePlatform
--ozone-platform=wayland
```

#### Bonus: Dark mode

Something that also isn't well-documented is dark mode on **Chrom{e,ium}** for Linux. This really has little to do with high resolutions but it is strongly related to the comfort of my eyes so I decided to include it here. You can add it the same way as the other options:

```sh
--enable-features=WebUIDarkMode
--force-dark-mode
```

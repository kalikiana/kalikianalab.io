---
title: Making music without the touch of a hand
subtitle: You only live once
date: 2023-01-10T21:53:01+02:00
tags: ["review", "theremin"]
images: ["20230107_105024.jpg"]
---

For a while I was harboring a fascination for an instrument which produced music following your mere motions. Unfortunately theremins are prohibitively expensive. Or so I thought until I discovered the [Open Theremin](https://www.gaudi.ch/OpenTheremin/) project.

<!--more-->

### I'd like to measure a gas, please

A hundred years ago [Lev Sergeevich Theremin](https://en.wikipedia.org/wiki/Leon_Theremin), commonly known as Leon Theremin, was working on an oscillator with the intention of measuring the dielectric constant of gases more accurately. While looking for more use cases of the principle including the first motion detector Theremin realized that adding an audio circuit made it possible to change pitch by moving your hand. It might be fair to say that Leon Theremin who's also known for [The Thing](https://en.wikipedia.org/wiki/The_Thing_(listening_device)) and the [mechanical TV](https://en.wikipedia.org/wiki/Mechanical_television) wasn't afraid to try completely new things.

### Can you make music without strings or keys?

What attracted me to start with wasn't the physics behind it or an interest in covert listening devices. I was watching a musical performance on stage without any spoilers on what I was going to experience. What I saw was a person who was moving their hands in the air to play a tune. It was magical. I had no idea what it was and how it worked, and if you've never heard of a theremin you might have an idea what I'm talking about. You see the antennas of course but it won't explain how that leads to sound. My classical education had included knowledge about string instruments, flutes and pianos. Keys and holes are relatively straightforward to operate for most people. Strings vibrate. Drums respond to pressure. But what if you never touch the instrument at all?

### Introducing the Open Theremin project

Enter a Swiss engineer by the name of Urs Gaudenz who was attending a festival around creative uses of technology. Gaudenz was fascinated with the theremin but found that existing kits that could be used to build one were interesting to fiddle with but flawed as serious musical instruments. Why shouldn't it be possible to build a fully functional electronic board to make a theremin? The original version was implemented as an Arduino shield. If you like to work with microcontrollers you've probably come across the Arduino which can be used to build a great many things. Before the Raspberry Pi it was probably the go-to device for most people getting into that space.

The current revision 4 has come a long way to the point where neither prior knowledge of programming nor soldering skills are required. At the same time both the hardware and software are completely open. For someone like me who's not afraid to mess with code but still clumsy with a soldering iron this is lowering the threshold considerably.

### What's in the box?

I opted for the Open Theremin v4 **Deluxe Bundle** which comes pre-assembled including a 3D-printed back cover, a stand and a USB type C cable in addition to the aluminum antennas and the theremin itself. For under 200€ before taxes[^1] this seems like a pretty fair deal and it's even more affordable if you don't mind assembling it yourself. Professional theremins usually start at 500€ and easily go into the 4 digits.

[^1]: I did have to pay 50€ for Swiss customs.

Setup starts with attaching the two antennas. There's a loop antenna which is used to control the volume and a pitch antenna composed of two parts. The included stand uses a standard screw meaning you can also use whatever tripod you already have. Power comes via USB type C. It's important to note that the theremin must be grounded so either you use a wall adapter that has ground or connect a wire to ground the instrument[^2], otherwise you'll get more noise than anything else. Remember that this is an instrument that works by measuring its surroundings. And of course to actually hear something you need an active speaker - in my case a portable bluetooth speaker with aux input.

[^2]: You can even connect the wire to yourself if you don't have anywhere else to attach it.

![Cover of the box saying OpenTheremin V4 by GaudiLabs](./20230105_141541.jpg)
![Open box revealing manual with introduction and picture of a setup theremin](./20230105_141646.jpg)
![View of the wrapped USB cable and tripod](./20230105_141728.jpg)
![Front of the theremin with Volume, Pitch, Register and Timbre knobs, Function and Ground to the bottom left and In/Out, USB-C, Audio and CV to the bottom right](./20230105_141844.jpg)
![Close-up of the right side of the theremin with one USB and two audio jacks](./20230105_141859.jpg)
![Close-up of the USB cable with thumb](./20230105_142130.jpg)

## Performance

I won't lie. This is not something you pick up and play regardless of prior experience with other instruments. It probably helps to have absolute pitch or decent relative pitch, but you'll have to develop a feel for the instrument. In this regard it's the same as other professional grade theremins. Any motion you make in the vicinity of the pitch antenna translates to point on the microtonal scale. In other words similar to a violin there's no exact tones but you need to find your octave and how it relates to the shape of your hand. Your play will always be a little different to how others play because your body is part of the instrument.

That said calibration is trivial as can be. You press the calibration button for 3 seconds and wait a minute for the theremin to analyze its environment. From that point you use the **volume** and **pitch** knobs to adjust the sensitivity as you would with e.g. a Moog theremin. The **register** knob further more determines the whole register you have at your disposal. **Timbre** modulates the curve that is produced.

![Cat approaching from behind to take a good look at a theremin](./20230107_105024.jpg)

### More than a theremin

There's no special effects built-in with the Open Theremin. This is in line with what you would expect in other theremins. However this is free software available under the GPL and [there's a MIDI implementation available](https://www.gaudi.ch/OpenTheremin/index.php/opentheremin-v4/midi-interface)! At least for now it's not included out of the box so you need to be comfortable flashing a firmware with MIDI support. If you're not familiar with MIDI you might be thinking of the internal speaker of a computer. That's not what it is. MIDI is how instruments speak to a synthesizer that can change the sound to a variety of instruments or effects. If you always wanted to play a harp or a saxophone but don't feel comfortable with those instruments that's how you do it![^3]

[^3]: And to an extent you can even cheat by changing how the theremin responds to your hands.

### Portability

As a side effect of how the Open Theremin is built it's actually incredibly portable and light. You can easily fit it in a small bag and run it off a USB powerbank, provided you keep the matter of grounding it in mind. As a minimalist I appreciate that and look forward to trying it out in the open air at some point. Setting it up in a park, hackerspace or at a conference is effortless by design.

### Do I have a verdict?

Do I recommend it? Absolutely yes. It's so easy to setup, ultra portable and very hackable. Am I seeing myself on stage? [Maybe not just yet](https://www.youtube.com/watch?v=_bh-KA22DyM).

![Theremin setup on a table with antennas on a tripod connected to a speaker with a complementary sleeping cat behind it](./20230107_125311.jpg)

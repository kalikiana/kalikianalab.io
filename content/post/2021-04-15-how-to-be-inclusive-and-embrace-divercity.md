---
title: How to be inclusive and embrace diversity
subtitle: You only live once
date: 2021-04-15T19:11:48+01:00
tags: ["semi-technical", "inclusiveness", "diversity", "gender", "terminology"]
type: post
---

### Do you like vanilla ice cream like everyone else?

Think of ice cream for a moment. Do you like chocolate? Do you like vanilla? Nobody can tell from looking at you what kind of flavor you like. People might assume that you like vanilla like everyone else. But maybe you don't like vanilla ice cream? What if you prefer strawberry? What if you don't like ice cream at all and everyone is having ice cream? 🍦️

Can you tell a person's gender from looking at them? No you can't. Maybe you never thought about gender that way. And maybe you don't know what it means. And maybe you don't need to know. Leave it up to each individual person what ice cream they like and what their gender is.

There's [a web comic about ice cream](https://everydayfeminism.com/2017/09/gender-is-kinda-like-ice-cream/) which plays out this comparison in more detail.

### Announcing your pronouns in an unobtrusive way

There's a way out. And it doesn't mean that you talk about ~~ice cream~~gender all the time. You can start by saying what your pronouns are. Put it in the bio of your blog or your social media profile. And add it to your email signature.

Add something like [he/him](http://pronoun.is/he), [she/her](http://pronoun.is/she) or [they/them](http://pronoun.is/they/.../themself). You can add pronouncs in brackets or in bold font, and optionally with [a link to Pronoun Island](http://pronoun.is/all-pronouns) which has a list of various possible genders and can even be extended easily by [filing a new issue on GitHub](https://github.com/witch-house/pronoun.is/issues). Some people specify more conjugations since the reader may not be familiar with some of them. If in doubt, just add a couple pronouns as regular text and keep it simple.

    What pronouns do you prefer?
    How would you like me to address you?

Last but not least you can always ask. You don't need to ask about someone's personal details. All you need is their pronouns. If you're already asking someone's preferred name you can follow up with the question of pronouns as well!

And, too, it's okay to ask for clarification. If you're not used to **they** or [ze](http://pronoun.is/ze/zir) you can ask how to spell it and what the forms are. That's never a bad thing because it shows that you care. It's not different to asking how to spell someone's name!

### Try and be conscious of gendered language

What's gendered language? Think about wether it's necessary to say "I need a man for this job", "We're looking to hire a developer. He needs to know Javascript." or "Hello ladies and gentlemen".

Just take a moment to ponder why you're putting weight on gender and wether that's necessary. You might be excluding some of the very people you want to address. When you could also say "I need someone strong for this job", "We're looking to hire a developer. They need to know Javascript." or "Hello everyone".

### Even user stories and jokes can be inclusive

So this is probably the silly section of this article. And it's about names that you might need when you want to make a joke or describe a situation without talking about an actual person. These are some of the names I would use when I need one. I make a point of using *unisex* names to avoid the typical gender bias that many people have in certain contexts. And I also avoid gendered pronouns for the same reason.

- Allison Average is a software developer.
- Cary Car can conceal their nose effectively.
- Dakota Developer doesn't do dodgyy donuts.
- Evelyn Emu emulates cats by pretending to be a dog.
- Jay hates jay walking.
- Kim is the sort of person to look out the window when it rains.
- Lee can't be bothered.
- Morgan hates mondays.
- Robin loves jay walking.
- Taylor Tester tries to test tapir toughness.

What's *unisex* naturally depends on where you are. You might not think of the name *Allison* as unisex if you've only met one or two persons called Allison with the same and for example they're both male. Or maybe you only know a female *Robin*. Or perhaps you think of *Dakota* as a genderfluid name because that's what your friend Dakota goes by.

**Fun fact**: Names like *Inge* or *Andrea* are considered exclusively male or female names in different countries. In Germany both would typically be considered female names. Talk to somebody from another country and you might be surprised what they think! I treat them like genered names and don't recommend using them as unisex names for that reason.

### Use inclusive terminology in a technical environment

If you're working with software or hardware you'll eventually be looking at **blacklists and whitelists**, **masters and slaves**. Did you ever wonder why technology is so fond of terms that can be rude if not offensive in other contexts? And perhaps that's something that needs to change.

The [Linux kernel](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=49decddd39e5f6132ccd7d9fdc3d7c470b0061bb) could do it. [openQA](https://github.com/os-autoinst/openQA/commit/1bf5dd06264c0fba47428f164b65413322925d24) could do it. [Ceph](https://github.com/ceph/ceph/pull/35902/) could do it. And so many more already did!

Take a moment and see if you can start using **blocklist**, **allowlist**, **primary**, **main**, **controller**, **host**, **denylist**, **allowlist**, **source** or **sink** to name some alternatives. You may find they are actually more expressive than the traditional terms!

In case you're reading this and thinking... but, why? It may surprise you to learn that this topic can be traced [as far back as 1994 in the music scene](https://www.jstor.org/stable/40327073). And software projects are only catching up relatively recently. The [Inclusive Naming Initiative](https://inclusivenaming.org/faqs/) has some great resources on this that I would recommend to check out!

### Sometimes questions of terminology can get heated

I consider myself a **Scrum Master**. If you're not familiar with agile methodologies, it describes a person who helps team members work efficiently but also comfortably. You would talk to me when a task is unclear and I'll offer to help out if you've got a problem. Good teamwork lies at the heart of a good project.

More recently [people raised the point of wether master is the best term here](https://www.scrum.org/forum/scrum-forum/40445/renaming-master-scrum-master) and suggested **Scrum Lead** as one of the alternatives because it doesn't imply a particular power dynamic - and actually scrum by design puts people on equal terms, and I don't put myself above anyone in the team because we all contribute different skill sets. Very quickly people argue what they think **master** stands for in this context and we get curious suggestions like the *Agile Clown* and the *Scrum Hooker* - which in a rugby context means being in the front row but transported here becomes ambiguous. Finally there's a demand to close the thread from someone claiming that participants in the conversation are "an over-politicized mob".

This is the sort of thing that detracts me and others from wanting to engage at all. I'm quite passionate about *scrum* and I think it adds a lot where I use it. But I don't know if I'm comfortable in the community around it.

### Is your company inclusive?

Finally there's the matter of **DEI** in a corporate environment. By the way if you've not heard the term it stands for *Diversity*, *Equity* and *Inclusiveness*. And this is about giving you the visibility you deserve. Let's have a quick look over these terms:

- **Diversity**: This can be gender, religion, sexuality, handicaps, cultural background or political views. All of what superficially makes a person.
- **Equity**: Not to be confused with the use in a financial context. Equity is freedom from bias. Fair distribution of available resources.
- **Inclusion**: Do you feel like you're at welcome and that you're represented and involved in important conversations?

I'm lucky to work for a company where that's being taken seriously. And it's not that it's taken for granted but it's being *acknowledged*. And, too, I know that I get the support to do something.

So maybe you feel like there's room for improvement in your company. And regardless of the specifics it's worth inquiring what is possible and what lies in the future. Even if it's a little thing. Like being given the green light to add your pronouns to your professional **email signature**:

    --
    Liv Dywan (she/ they)
    QA Automation Tool Developer for Software Maintenance

That's what mine looks like now (omitting the full company name and address here). Pronouns just go in parens after my name.

---
title: Setup a blog with GitLab Pages and Hugo
subtitle: You only live once
date: 2021-02-12T15:01:07+01:00
tags: ["howto", "gitlab", "hugo", "opensuse"]
type: post
---

### Blogging the lazy way

**Note:** This little guide is focussing on *GitLab*, you can also [setup a blog with GitHub Pages](https://bzoltan1.github.io/setup-a-blog-with-github-pages-and-hugo/) if that's your preference.

Allow me to introduce you to [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/). In short, you get a free git repo here with support for [Hugo](https://gohugo.io/) which is a static website generator based on Markdown. And the end result will show up on `username.gitlab.io` without your having to worry about domains or hosting. Sounds good? Let's see how to set it up quickly.

**First step**, make sure you have an account at [gitlab.com](https://gitlab.com/) or create one now if you don't. Conceptually you could of course replicate these steps on a self-hosted GitLab instance, GitHub or another hosting option of your choosing - I will be using GitLab in this howto. Consequently [🍴Fork](https://gitlab.com/pages/hugo/-/forks/new) the default template and select your user account. Afterwards you should end up at what looks pretty much the same (so don't be tempted to fork your own fork again) but with your username in the URL.

**Second step**, run the pipeline once. We haven't made any changes to the template at this point. This is only to confirm the new blog is in place. This is going to update automatically later.[^1] But it's a good opportunity to see how the blog is built going forward. Under *Settings > CI/CD* you will find *Pipelines* which will then say *There are currently no pipelines*. Hit the green *Run Pipeline* button to change that. And once more, *Run Pipeline* in the next screen. Get a cup of tea, coffee or perhaps a glass of water whilst the pipeline is running - it's important to stay hydrated 🍵👍.

**Third step**, go to *Settings > General*. Go down to *Advanced > Change path > Path*. Rename your repository from `hugo` to `username.gitlab.io` where the `username` portion is your account's username. This will make this blog the default for your toplevel domain. The change is instant - if the domain gives you a 404 error, check for typos and try again. Of course if you wanted the blog in a subfolder instead, you can also use a name like `blog` or keep the default `hugo` instead. Just take this into account later when filling in metadata.

### Posting the hugo way

Finally let's edit our new blog. Depending on your test there's two ways to do this. You can clone the repo and use your favorite local editor. Assuming basic knowledge of *git* the **Clone** button should give you a URL which you use like this:

```bash
git clone git@gitlab.com:kalikiana/hugo.git
```

You can also just use the *web interface* and use ➕ *New file* to add a file. For the purposes of setting up and editing your page both works just fine.

Adding a new post, which is the most important part, is as simple as adding a new file to the folder `content/post`. Name the new file something like `2021-02-10-from-zero-to-hero.md`. The pattern here is the date of the post in ISO format, i.e. `YYYY-MM-DD` with the month coming second. As for the post itself:

```yaml
---
title: From zero to hero
subtitle: You only live once
date: 2021-02-10T09:12:17+01:00
tags: ["yolo", "hero", "opportunities"]
type: post
---
```

The metadata is probably pretty self-descriptive. The date contains a time here. On a linux system you could use `date --iso-8601=seconds` to get the current date and time in this format, which is `YY-MM-DDThh:mm:ss` with a `T` in the middle and your local timezone in the end. Tags will be used throughout your site as you use them. This is written as a *YAML* document, so the list of tags resembles an array in Javascript. Picking two or three is proably a good idea so readers can easily browse or subscribe by topic.

~~~Markdown
### Intro

et situs vilate inis et avernit.

You can [add external links](https://www.youtube.com/watch?v=dQw4w9WgXcQ) using square brackets here, make font **bold**, *cursive* or ~deleted~. The format supported here includes exteions from [GitHub Flavored Markdown](https://github.github.com/gfm) which you may already be familiar with, footnotes[^1] and pictures:

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

And of course source code:

```javascript
console.log('hello world');
```

- And
- lists

[^1]: Strictly speaking this isn't standard Markdown either.
~~~

You can also browse the existing posts for further inspiration. Saving this first post is as simple as creating the file via the web editor or via a few simple commands:

```bash
git add -a
git commit -p -v
git push origin HEAD
```

Whichever way you chose, take a little break while your page is automatically being updated. If you want some reassurance, consider a little exercise. Remember the *pipelines* mentioned earlier? See if you remember how to find them and look at what's happening there. 🤓

### Configuring your new blog like a boss

To wrap things up you will want to also edit `config.toml` at the root of your repository:

- `baseurl` should use your account's username, i.e. `https://username.gitlab.io/`
- `title` and `subtitle`
- The `[Author]` section contains various fields for social networks that you can fill out or remove as needed.
- Review the `[[menu.main.]]` sections. My suggestion would be to drop everything except for *About* and *Tags* for now.

To add a bit of a personal touch and make the site more identifiable next to a gazillion other tabs in the browser, you may want to change both `logo` and `favicon` to `"avatar.png"` and drop an image file by that name into the `static` folder (be sure to `git add` the new file).

That leaves deleting the file `content/_index.md` (or you can fill in something to show on the front page) and updating `content/page/about.md` for good measure.

#### A little work-around for theming ABI changes

**Note:** Due to [a bug caused by an upstream behavior change](https://gitlab.com/pages/hugo/-/issues/42) you will want to edit `themes/beautifulhugo/layouts/index.html` and change

    {{ $pag := .Paginate (where .Data.Pages "Type" "post") }}

to read

    {{ $pag := .Paginate (where site.RegularPages "Type" "in" site.Params.mainSections) }}

because otherwise the homepage only says **Posts** rather than listing all of your posts.

### Can I get that syndicated, please

Of course to make the most of this, I will recommend one more step. And this depends a bit on your audience. You may want to find a planet to share your posts with a wider audience automatically. Users won't have to be reading your blog, but could be browsing topics in a bigger collection.

If you're looking to setup something new, [planet.rb](https://github.com/feedreader/planet.rb/blob/master/planet.rb) or [worldcrab](../2021-08-26-how-to-aggregate-your-blogs-with-the-worldcrab) might be worth a look. To add your blog to **planet.opensuse.org** you can follow these simple steps:

- Login or sign up with GitHub
- Fork [openSUSE/planet-o-o](https://github.com/openSUSE/planet-o-o)
- Edit `https://github.com/username/planet-o-o/blob/master/planet.ini` where `username` is your account's username

The section you want to add looks something like this:

```ini
[username]
  title    = The title of my blog
  feed     = https://username.gitlab.io/index.xml
  link     = https://username.gitlab.io
  location = en
```

Push this to a branch and open a PR. Once this is reviewed your blog will show up on [Planet openSUSE](https://planet.opensuse.org)!

### Here you go

These steps should have set you up for very easy, as in *create-a-new-file easy*, blogging with about the effort needed to file an issue, describe a merge request (or pull request) or write a book (on HackMD.io for example). I hope you're enjoying your new blog!

[^1]: It's a bit of a buzzword, but gitops is really cool and you can automate server deployment, builds and more that way.

---
title: The bare minimum to keep dot files in git
subtitle: You only live once
date: 2022-04-27T19:08:11+01:00
tags: ["howto", "git"]
---

What do you do to backup files in your home? How do you share them and keep track changes? Version control to the rescue!

<!--more-->

### Let's setup a bare git repo

In keeping with the goal the blog to focus on getting things done I'm not going to get into a long explanation of what a bare repository is. In this context bare means that we'll move the git folder out of the way. We don't want every file in **~** to appear as though it may be tracked in the same git repo. And a `git add` or `git commit` in a subfolder should not detect a repo unless there is one in that folder.

    git init --bare $HOME/.dotfiles -b main
    git remote add origin git@github.com:example/dotfiles.git

In your `~/.bashrc` or equivalent you want this alias:

    alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

In my case the folder itself is called `~/.dotfiles`. You can omit the `.` if you prefer and it will work the same way so long as you use the according alias.

### How do we use this?

Managing dot files is pretty simple really:

    dotgit add ~/.bashrc
    dotgit commit -m "Update dot files"
    dotgit push origin main

This is ultimately just git, no more, no less.

### Adding another machine

This obviously becomes most useful once you can replicate the setup easily on a new machine, or if you need to wipe your home:

    git clone --separate-git-dir=$HOME/.dotfiles git@github.com:example/dotfiles.git dotfiles-tmp && rsync --recursive --verbose --exclude '.git' dotfiles-tmp/ $HOME/ && rm -rf dotfiles-tmp

The little dance with a temporary folder is so we can have the files in an existing ~ rather than a subfolder.

These three commands ideally live somewhere easy to copy. My suggestion is the README.md of the same repository so that you can open this in a web browser:

```
# Dot files

See [The bare minimum to keep dot files in git](https://kalikiana.gitlab.io/post/2022-04-27-the-bare-minimum-to-keep-dot-files-in-git/)

## Setting up a new machine

    git clone --separate-git-dir=$HOME/.dotfiles git@github.com:kalikiana/dotfiles.git dotfiles-tmp && rsync --recursive --verbose --exclude '.git' dotfiles-tmp/ $HOME/ && rm -rf dotfiles-tmp

## Updating dot files

    dotgit add ~/.bashrc
    dotgit commit -m "Update dot files"
```

That's. Enjoy your new dotfiles.

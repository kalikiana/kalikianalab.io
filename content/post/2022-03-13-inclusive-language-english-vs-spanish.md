---
title: Inclusive language - English vs Spanish
subtitle: You only live once
date: 2022-03-13T13:32:34+01:00
tags: ["semi-technical", "inclusiveness", "diversity", "gender", "terminology"]
---

They? Them? elle? le? Why is gender neutral Spanish so difficult and how do you make it work?

<!--more-->

### What are your pronouns?

An innocent question which can make you feel very welcome or lead to an icecold surprise. I think many people simply don't know what this is about. Let's start from the beginning, though:

Case in point, say you go by **they/ them**. What's that mean, though?

Cary likes cats. **They** like cats. Tell **them** about your cat. **Their** cat is called Pyewacket.

The singular they has been around since the 14th century. Shakespeare was no stranger to it. Plural they isn't even much older. Although the generic he is being defended as a gender neutral term to this day.

### Qué pronombres usas?

In Spanish this isn't quite so clear-cut. Historically the language has never known ambiguous pronouns. That is, until very recently:

A Dakota le gustan los gatos. A **elle** le gustan los gatos.

Let's take a closer look at the cases here:

|Question|Pronoms|Exemple
|-|-|-|
|Quién?|elle|A elle le gustan los gatos|
|Quién plural?|elles|A elles les gustan los gatos|

What if, you ask, the gender of the person themself is ambiguous? Why thank you, there is a solution for that as well:

|Pregunta|Ejemplo|
|-|-|
|De yo?|Une amigue míe.|
|De tu?|Une amigue tuye.|
|De elle?|Une amigue suye.|

And not to forget Spanish has gendered articles:

|Pregunta|determinado|indeterminado|
|-|-|-|
|Quién?|le amigue|une amigue|

### Lose the grammatical gender

You may have noticed my use of **amigue** in the examples. Many words in Spanish require gender to be specified. In other cases the grammatical gender is oblivious to the gender of the individual, or there is no established noun.

|masculino|feminino|neutro|
|-|-|-|
amigo|amiga|amigue|
hermano|hermana|harmane|
padre|madre|(un padre)|
colegua|colegua|colegua|

And of course there's demonstrative terms:

|masculino|féminin|neutro|
|-|-|-|
este|esta|-
todo|toda|tode
todos|todas|todes

### The variations

Like in English, there's other pronouns or variants with some differences. I'm not going into detail because my motto for this blog is, feel free to take my example and run with it. This is a pragmatic outlook, not an overview of everything out there.

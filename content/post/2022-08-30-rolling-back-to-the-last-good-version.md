---
title: Rolling back to the last good version
subtitle: You only live once
date: 2022-08-30T14:25:01+02:00
tags: ["howto", "opensuse", "flatpak", "microos"]
---

Do you love to install the latest and greatest? Try out new and interesting things? Except it might break for some reason. If only you had a time machine...

<!--more-->

### The case of an upgrade that breaks your extensions

My main email client is Thunderbird. There's things I love about it and things not quite to my liking, but at the end of the day it's what keeps me productive when I need to manage several email inboxes for work, projects I'm involved in and personal life. Calendars are also integrated here which I would otherwise have to look up in various places. I need appointments to be saved somewhere, lest I forget.[^1]

[^1]: This is why I ask people to send me invites or at least a message with written details. Keeping arbitrary numbers in my head is not something I'm good at.

Of course I use [Thunderbird from Flathub](https://flathub.org/apps/details/org.mozilla.Thunderbird). I get the latest version, it's isolated from the rest of my setup and it doesn't require changes to the root filesystem.

Thunderbird updated to version 102 the other day, with shiny new UX changes and I loved the look. Until I realized it silently broke my calendar integration and all of my appointments were outdated 😑️

### Fret not, let's simply go back for now

On the bright side going back to a working version was trivial. I knew the previous version was working fine, so I went to find out the commit hash:

    flatpak remote-info --log flathub org.mozilla.Thunderbird | more

What did I do with this? I reverted to the previous version:

    flatpak update --commit=8807434f7bad0c28c0ae1a84de46f43457e01cadef7c02755fab3be26474708c org.mozilla.Thunderbird

Of course Thunderbird didn't like this because it had already changed my config files:

**You have launched an older version of Thunderbird**

This, too, could be solved, though:

    flatpak run org.mozilla.Thunderbird --allow-downgrade

After this Thunderbird is once again happily working with my beloved addons.

### Prevent updates from being pulled in automatically

Your app will still continue to receive updates and automatically move to the next available version. This is great in general because you don't need to worry about having to pull in new versions. And it's safe because as we just found it's trivial to go back.

Just one flaw. If the new version brings back incompatible changes the only sensible choice may be to stay on a known working version for the time being. So let's consider this:

    flatpak mask org.mozilla.Thunderbird

That's it. The app won't be updated automatically from here on. And incidentally this works for runtimes, too.

Of course eventually we may want to be back on the latest version, in which case we can unlock updates again:

    flatpak mask --remove org.mozilla.Thunderbird

And just in case we can't remember what we configured previously it's also possible to list any such locked packages:

    flatpak mask

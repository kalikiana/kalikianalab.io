---
title: Inclusive language - English vs French
subtitle: You only live once
date: 2022-03-08T21:04:45+01:00
tags: ["semi-technical", "inclusiveness", "diversity", "gender", "terminology"]
---

They? Them? iel? ellui? Why is gender neutral French so difficult and how do you make it work?

<!--more-->

### What are your pronouns?

An innocent question which can make you feel very welcome or lead to an icecold surprise. I think many people simply don't know what this is about. Let's start from the beginning, though:

Case in point, say you go by **they/ them**. What's that mean, though?

Cary likes cats. **They** like cats. Tell **them** about your cat. **Their** cat is called Pyewacket.

The singular they has been around since the 14th century. Shakespeare was no stranger to it. Plural they isn't even much older. Although the generic he is being defended as a gender neutral term to this day.

### Quels sont vos pronoms?

In French this isn't quite so clear-cut. Historically the language has never known ambiguous pronouns. That is, until very recently:

Lee aime les chats. **Iel** aime les chats. Parle **ellui** de ton chat. **Iels** sont ami·es.

Let's take a closer look at the cases here:

|Question|Pronoms|Exemple
|-|-|-|
|Qui?|iel|Iel aime les chats|
|Qui pluriel?|iels|Iels aimes les chats|

The agreement with the object remains as usual, according to the grammatical gender:

son chat
sa chatte

What if, you ask, the gender of the person themself is ambiguous? Why thank you, there is a solution for that as well:

|Question|Exemple|
|-|-|
|De moi?|C'est mo copaine.|
|De toi?|C'est to copaine.|
|De lu?|C'est so copaine.|

And not to forget French has gendered articles:

|Question|déterminé|undéfini|
|-|-|-|
|Qui?|lu copaine|an copaine|

### Lose the grammatical gender

You may have noticed my use of **copaine** in the examples. Many words in French require gender to be specified. In other cases the grammatical gender is oblivious to the gender of the individual, or there is no established noun.

|masculin|féminin|épicène|
|-|-|-|
ami|amie|ami·e|
frère|sœur|(la fratrie)|
père|mère|parent|
copain|copine|copaine|
docteur|docteur|docteur|

And of course there's demonstrative terms:

|masculin|féminin|épicène|
|-|-|-|
ce|cette|çu
tout|toute|touz
tous|toutes|touze
quel|quelle|quéal|

### The variations

Like in English, there's other pronouns or variants with some differences. I'm not going into detail because my motto for this blog is, feel free to take my example and run with it. This is a pragmatic outlook, not an overview of everything out there.

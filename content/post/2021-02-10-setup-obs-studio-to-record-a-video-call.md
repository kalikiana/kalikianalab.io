---
title: Setup OBS Studio to record a video call
subtitle: You only live once
date: 2021-02-10T16:11:07+01:00
tags: ["howto", "obs-studio", "recording", "video call"]
type: post
---

### Let's preserve that precious knowledge

Virtual workshops are a thing. And because they are virtual, it's easy to make a recording that you can share with everyone interested. Or maybe you're organizing a video call and some of those beautiful people can't make it. Let's see how to do this with no prior knowledge.

### Get OBS Studio up and running

```bash
flatpak install com.obsproject.Studio
```

Since I like to keep my base system as lean as possible I'm quite happy to use the flatpak here. There are [community packages for obs-studio](https://software.opensuse.org/package/obs-studio), though, and [packman](http://packman.links2linux.org/package/obs-studio) as has been pointed out to me.

Once you start up *OBS Studio* you'll be greated with a wizard that wants to confirm what you'll be using OBS for. I assume **Optimize for recording** is what I want here. I don't know what the specifics are but I'll consider first of all that I want to record the session and upload it somewhere later. My aim is to keep it very simple and go with defaults. For the record, you can easily change this later or re-create your profile if you tried out OBS before, and nothing is carved in stone here.
*Video Settings*, again no idea what this actually means so I'm just hitting *Next* here.
The last step took a few minutes until it ended up with *768p/480p*.[^1]

### Let's setup our sources

Use the ➕ button below the *Sources* list and select **Window Capture, XComposite**. This is analoguous to sharing a window on a call. If like me you try and keep the chrome out of presentations, the easiest option is to use just one document or website open in it. Any tabs you open or switch to in that window will be visible, and window decorations and toolbars will clutter the viewport - you can avoid that by using *fullscreen* or by moving the canvas to hide chrome - the viewport acts like an image in Inkscape, GIMP or Photoshop which means you can move sources around freely. It's a bit clunky that way as OBS will be smart about it and fallback to another window. So you kind of have to double-check the output before you hit that *Start Recording* button in the bottom right of the main window. But not worse than it would be if you just shared the window in a video call.

**Note:** While it has *XComposite* in the name, it works fine with *Wayland*.

Also, and this is something I didn't realize right away, you may need to fix the colors in your video. Since I use a lot of colorful lighting in real life I thought maybe that was affecting the video somehow. In the *Properties* of the window capture source there is a ☑ *Swap red and blue* option. Without this I was getting some very interesting color filters. I'm guessing this has to do with the particular webcam. Fortunately easy to fix once I figured this out.

Next one up is **Audio Output Capture (PulseAudio)**, which can be added via ➕ just like the video capture. I went for *Default* here so that whatever I say or hear ends up in the recording. No surprises here at least.

To test this setup I'm using an *empty* Jitsi video call, or in other words a room that nobody else is joining, so I can replicate a call without asking coworkers to pose for me. I'm also using [a test video](https://www.youtube.com/watch?v=dQw4w9WgXcQ) and feeding that into the *YouTube* feature. Although I use this very rarely otherwise, it will reassure me that sound that's not coming from my mic gets recorded.

**Note:** While it has *PulseAudio* in the name, it works fine with *PipeWire*.

The third step is completely optional. If you want to be super *trendy* you will want a custom background showing you on a beach, in a space station or sitting in a fake office with Bernie next to you. The most obvious way to do that is by adding an *Image* source. That is you once again hit the ➕ icon in the *Sources* list, select **Image** in the popup menu, optionally give it a unique name and pick an image file. Be sure to move the new image down in the list by hitting the 🔻button repeatedly. Select your window capture and click *Filters*. Hit ➕ and choose **Chroma Key**.

That's pretty much it, you're ready to go.

### Further considerations

If you won't or can't use a screen, there's apparently a plugin called [obs-virtual-background](https://github.com/axiak/obs-virtual-background) which uses [bodyPix](https://storage.googleapis.com/tfjs-models/demos/body-pix/index.html) to erase the real background without a screen. I didn't spend too much time investigating that further, though, since this exceeds what I'd consider quick and easy to setup. If somebody tried this out, I'd be interested in the results. Maybe this could be packaged along with OBS in the future.

It is worth noting that all dialogs in OBS look like modal dialogs and seems like they are because closing a window sometimes give you a modal Discard/Cancel/Save dialog regardless of having changed something. But aside from that you can keep everything open and tweak your setup live, with changes applied instantly. Provided you have enough screen real estate or an extra monitor.

### And that's it

Assuming you've played with this a little bit and know the basics, you should have a working setup. If you haven't already started doing that now you can hit the *Start Recording* button in the bottom right to start a recording. The *Stop Recording* button is located in the same place. By default recordings are found in the user's home, saved as *Matroska* (mkv). That's something you can change in the settings if needed. Now to plan a video to record. 😉

[^1]: For the purposes of blogging about it, I re-ran the wizard in a new profile and the effective resolution was higher. Maybe the test is unreliable?

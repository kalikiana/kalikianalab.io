---
title: How to help test-drive zypper
subtitle: You only live once
date: 2021-08-31T11:15:53+02:00
tags: ["zypper", "opensuse", "howto"]
---

As you may or may not know [libzypp](https://en.opensuse.org/Portal:Libzypp) is what powers **zypper** and **YaST**, which are the packaging frontends on **openSUSE**. Chances are, you've used both. But have you ever tried a development version or enabled unstable features?

<!--more-->

### You are using Tumbleweed, aren't you?

If you're not using the latest [openSUSE Tumbleweed](https://get.opensuse.org/tumbleweed/) the following options won't be available to you out of the box. Don't worry, there's still options for you. So I'm ging to briefly touch on that before moving on to the fun stuff.

#### Tumbleweed in containers

Perhaps unsurprisingly you can use containers. Your host might be Leap or MicroOS. That's not going to stop you! Typically I would recommend [toolbox](https://github.com/kubic-project/microos-toolbox) or [podman](https://podman.io/) for the purpose:

    toolbox -u -i registry.opensuse.org/opensuse/tumbleweed:latest
    podman run --rm -it registry.opensuse.org/opensuse/tumbleweed:latest

I also wrote [a blog post about developing in Toolbox containers](../2021-02-22-developing-in-toolbox-containers) which goes into more detail if you've not used Toolbox before.

#### zypp from bleeding edge development repos

This is definitely an option for the brave souls out there, although it's not going to be your first choice if you don't have backups[^1]:

[^1]: Honestly you should have backups in any case. Because something will break sooner or later.

```bash
sudo zypper ar --refresh https://download.opensuse.org/repositories/zypp:/Head/openSUSE_Tumbleweed/zypp:Head.repo
sudo zypper dup --from 'https://download.opensuse.org/repositories/zypp:/Head/openSUSE_Tumbleweed/' --allow-vendor-change
```

If you want to test straight from git with the option to contribute patches you can also get and build [from git](https://github.com/openSUSE/libzypp):

    git clone git://github.com/openSUSE/libzypp.git

You may want to subscribe to [zypp-devel@lists.opensuse.org](https://lists.opensuse.org/manage/lists/zypp-devel.lists.opensuse.org/) and join the `#zypp` IRC channel on **Libera**.

### Brand new transactions are brand new

Traditionally zypper shells out to rpm for each operation. Although it may look smooth and slick, behind the scenes it's a little less straight-forward. And it can be slow. So how about we try out that new backend which merges all transactions into one?

    ZYPP_SINGLE_RPMTRANS=1

There's a couple of drawbacks with this:

- Unless you're on Tumbleweed you might run into [boo#1189788](https://bugzilla.opensuse.org/show_bug.cgi?id=1189788)
- If you're enabling it system-wide you'll probably break YaST and PackageKit. The fix is simple, though. Only use `ZYPP_SINGLE_RPMTRANS=1` in individual commands.

### New hot potatoes are hot

There's also the matter of downloads. Parallel downloads is a new feature, thanks to a new asynchroneous backend. The work is not complete yet so consider this a teaser for how fast it's going to be when it's done. 😏️

```bash
ZYPP_MEDIANETWORK=1 
```

You can enable the variable in your profile, login shell or just for individual commands as per your preference. Of course, keep in mind that it's not stable yet.

### What if zypper gets stuck and something breaks?

Head over to ``zypp`` on the **Libera** IRC channel, or better yet file a bug report on [bugzilla.opensuse.org](https://bugzilla.opensuse.org/enter_bug.cgi?classification=openSUSE) under **openSUSE Tumbleweed/libzypp**.

Happy testing!

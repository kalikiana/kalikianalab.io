---
title: Decent picture in meetings with Anker PowerConf C300
subtitle: You only live once
date: 2021-09-21T19:53:53+02:00
tags: ["review", "webcam"]
---

What I needed was a webcam with a decent picture that can sit on top of a monitor at a sensible angle. Usable out of the box rather than tweaked to taste. And ideally not too annoying to look at. Here's what I found.

<!--more-->

### Why do I need a webcam anyway?

I needed a sensible webcam that wouldn't prevent me from working [the way that I consider more healthy](../2021-04-20-working-out-of-and-in-your-homeoffice). A standing desk is something I've used on and off for several years now. And one thing that's always been tricky is the webcam situation. It's possible to grab a chair and move to a lower desk in front of the laptop screen and that's been my solution before. These days I have a significant number of regular meetings as well as ad-hoc video chats, and I don't want to sit all day just to use a webcam.

### What's in a webcam?

I wanna say I don't care about the picture quality. Then again the reason I was looking for a new webcam was so I can enjoy more ad-hoc chats and pair debugging with colleagues. So perhaps it's worth seeing if I can find one that's not terrible around the 50€ mark[^1]. Spoiler, it turned out to be a little more and I think it wasn't a bad decision.

[^1]: I noticed this unintended pun after re-reading the article. Of course the euromark was never a thing and I'm not trying to accomodate readers from the past either.

![webcam box front](./20210908_110244.jpg)
![webcam box back](./20210908_110253.jpg)

### What's in the box?

The PowerConf C300 comes in your average modern box, like you expect from streaming sticks, phones and earphones that appeal to wider audiences. It mentions a privacy slider, a USB type C cable and a USB type C to A adapter. I hadn't made that a hard requirement from the start since I was likely going to plug it into a dock anyway, but I'm loving how type C is the default here. Not to mention the cable is fullay detachable so if it breaks I grab a new one from my cable box and I'm fine.

![webcam box open](./20210908_110334.jpg)
![webcam box contents](./20210908_110614.jpg)
![webcam front](./20210908_123409.jpg)

## Big sibling is watching you

I was checking a few reviews before. A couple mentioned the optional privacy slider briefly suggesting it looked ugly and wasn't useful. And I rather liked the idea. So I'm going to explain what you really get and you can decide if it looks too bad for the privacy gain.

What you get is actually very similar to the sliders[^2] many use on laptop webcams, except it's a bigger since the lens is significantly bigger since a laptop webcam. It's also adhesive on the back, meaning you peel the foil off and stick it on there. Chances are you'll get it in almost the right place. If you're feeling a little bored you can try and see it in the picture. There's two in the box by the way, in case you need to replace it. Sure, Anker could've implemented a proper sliding mechanism here. Might've helped them keep the price low in which case it worked in my favor.

[^2]: I'm not talking about food here. Maybe you should have some burgers today.

![webcam with open slider](./20210908_123552.jpg)
![webcam with slider closed](./20210908_123619.jpg)

### The angle makes the difference

The C300 can be shifted forward and back as well as rotated left and right. Put on top of a monitor, which is probably the most common case, it can naturally sit anywhere along the width of it. I find my sweet spot is close to the second third since I don't stand exactly in front of the monitor, and I angle it a little forward, too. Pretty straightforward to test out until it works. It's also tight enough it won't easily fall off.
As a freely mobile camera you can actually pretty much show off the entire room you're in. The provided cable is plenty long and it could even be used to record video in totally different angles. Tripods using the familiar screw can be used, too.

### Picture and sound quality

If your mindset was one where you'd also say you don't care about picture quality like me, you'll probably be impressed with what you're getting here. Assuming you were interested in something decent without the budget for it, you might find an unexpected level of quality at your kind of price range. Most importantly what sold me was the picture in low-light settings.
I didn't announce my new setup ahead of time but just switched to it. And a bunch of people independently pointed out the better quality.

The C300 has a built-in mic which means it pairs very well with a headset with no mic, or in case your headset mic is too noisy. It's unfortunately quite common in my experience that colleagues sound as though they sit in a server room or in a 'thopter, and I might joke about it but it's really important that we can hear each other properly. Case in point, I tested the mic on a fair amount of calls and the feedback was good, that is nobody even commented on the sound quality. Using it with speakers also works very well (it should go without saying notifications need to be muted when doing that).

I'm not going to analyze the hardware in-depth here and I don't have much to compare with, so instead I recommend watching [Anker PowerConf C300 Webcam im Test (German)](https://www.youtube.com/watch?v=cQAhElpdA2s) or [Anker Powerconf C300 (English)](https://www.youtube.com/watch?v=DLfM6E768rI). I can't speak to using the proprietary config tool either, but if you're interested in that you'll find it covered there, too.

### What's not to like?

The webcam has no speakers, should that somehow be important to you. My myriad of audio devices isn't lacking in that department so that's fine by me. You can also argue that the privacy cover looks like an after-thought - but then again they did think about it and included it, and it works pretty well in practise. I should also mention that there's no 4K to be found here.
---
title: Getting a new Perl module into openSUSE
subtitle: You only live once
date: 2021-02-24T16:31:04+01:00
tags: ["howto", "opensuse", "packaging"]
---

### There is this Perl module on CPAN that looks really handy

So, I want to use [Devel::Cover::Report::Codecovbash](https://metacpan.org/pod/Devel::Cover::Report::Codecovbash) which is a *Perl* module. To test out how it works of course I can simply install it off *CPAN*:

    cpanm -nq 'Devel::Cover::Report::Codecovbash'

I can use it just fine like this. To be sure, installing modules as a user `$PERL5LIB` needs to be set accordingly. Say I spent some time trying this out and decided that I want to *depend* on this package. More concretely to use it in **openQA** upstream I need it to be available as a package on *openSUSE*.

### How do I get this packaged then?

Apparently the project [lives on GitHub](https://github.com/houseabsolute/Devel-Cover-Report-Codecovbash) but there's no *spec* file there that I could build an *RPM* package from 🤔 This is where the magical [cpanmirror](https://build.opensuse.org/users/cpanmirror) comes in which has [perl-Devel-Cover-Report-Codecovbash](https://build.opensuse.org/package/show/devel:languages:perl:CPAN-D/perl-Devel-Cover-Report-Codecovbash) in it.

### Submission to DLP

I was going to *explain* how to submit the generated package to *devel:languages:perl* but it turns out [it's already there](https://build.opensuse.org/package/show/devel:languages:perl/perl-Devel-Cover-Report-Codecovbash). Well, I still got my package, right? 🤓

Just one thing that was missing that you want to do is the [meta description](https://build.opensuse.org/package/meta/devel:languages:perl/perl-Devel-Cover-Report-Codecovbash). It's worth checking that not only the *spec* file is correct but also the description here.

### Let's get this package into the factory

To be able to use the package elsewhere it first needs to go into [openSUSE Factory](https://en.opensuse.org/Portal:Factory). That's not a distro in the general sense but rather the foundation of *openSUSE Tumbleweed*. There's no freezes, no stability guarantees and no *QA* on it so you wouldn't want to install it as-is.

In the web UI you can use **Actions**, **Submit Package** to start a submission. Enter `factory` in the freeform text field and select `openSUSE:Factory`. Add a motivation for your request, such as `I would like to be able to use this as a dependency of openQA packages`.

Now wait for automated checks to run and give maintainers a chance to review.

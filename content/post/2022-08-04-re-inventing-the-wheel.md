---
title: Re-inventing the wheel
subtitle: You only live once
date: 2022-08-04T08:27:03+02:00
tags: ["howto", "openqa", "docker"]
---

[openQA](https://open.qa) test distributions can become quite big. Usually test modules and helper functions live side-by-side. What if you could easily split things up into multiple repositories?

<!--more-->

### What's wrong with having everything in one place?

A test distribution contains everything needed to run tests in openQA, or [with isotovideo individually](../2022-03-16-Running-standandalone-tests-with-isotovideo). That's fine and dandy if you're starting out. Eventually you may want a different repo for needles. Code shared between test modules probably still lives in one growing folder at this stage. And some test code may be duplicated between test distributions, making it difficult to maintain. Freely being able to split out test modules or helper functions becomes tempting.

### The third wheel

A **wheel** in the context of openQA is basically a repository that adds to an existing test distribution. Also it can be used in several distros using the same repo. The structure of it is a subset of what a full test distribution provides.

In order to start using wheels all you need is one extra file called `wheels.yaml`:

```yaml
version: v0.1
wheels:
  - os-autoinst/os-autoinst-wheel-launcher
```

If this file exists in `CASEDIR` the repos will be cloned before any test module is executed. `WHEELS_DIR` can be used to override this location explicitly.

```yaml
  - https://github.com/os-autoinst/os-autoinst-wheel-launcher
```

You might've already guessed it. For convenience it's enough to specify the namespace and the name of the repository, and it'll be looked up on GitHub. But a full URL can also be used. Of course branch names can be specified, too, which makes for easy vendoring:

```yaml
  - os-autoinst/os-autoinst-wheel-launcher#feature
```

This is the same syntax that can already be used for `CASEDIR` and `NEEDLES_DIR`.

### Building a wheel from scratch

This part is pretty straightforward and probably entails 3 dedicated steps:

1. Create a folder `lib` which contains re-usable Perl modules.
2. Create a folder `tests` which contains re-usable test modules.
3. Add a GitHubAction workflow to get CI for your wheel.

A file `lib/OpenQA/Wheel/Foo.pm` might contain something like this:

```perl
package OpenQA::Wheel::Foo;
use Mojo::Base 'Exporter', -signatures;

use testapi qw(type_string);

our @EXPORT_OK = qw(type_super_slow);

=head1 introduction

=for stopwords os autoinst isotovideo openQA

This openQA wheel provides super slow typing.

It is meant to be added to your distribution's wheels.yaml.

=cut

=head1 test API

=head2 type_super_slow [ slow_text => <$string> ]

The text "super slow" will be typed, which can be customized by specifying `slow_text`. Other arguments will be ignored.

=cut

sub type_super_slow (%args) {
    type_string $args{slow_text} // "super slow";
}

1
```

Next you might create a file `tests/wheels/foo.pm` looking something like this:

```perl
use Mojo::Base 'basetest';
use OpenQA::Wheel::Foo 'type_super_slow';

sub run {
    type_super_slow();
};

1
```

Finally a file `.github/workflows/isotovideo.yaml`:

```yaml
on: [push, pull_request]
jobs:
  isotovideo:
    runs-on: ubuntu-latest
    steps:
      - uses: actions/checkout@v2
      - uses: kalikiana/isotovideo-action@main
        with:
          schedule: tests/foo/bar
      - uses: actions/upload-artifact@v2
        with:
          name: Test results
          path: .
        if: always()
```

And that's all you need. The schedule is given explicitly since unlike a test distribution the wheel doesn't contain a `main.pm`. It's the same as what the test variable `SCHEDULE` expects.

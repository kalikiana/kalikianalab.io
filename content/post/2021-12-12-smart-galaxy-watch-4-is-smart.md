---
title: Smart galaxy watch 4 is smart
subtitle: You only live once
date: 2021-12-12T23:13:03+01:00
tags: ["review", "wearable"]
---

The Galaxy Watch 4 is a bit of an upgrade for me. Notifications with unicode, emojis und videos on the watch. I'm used to Xiaomi - I expect to trade two weeks worth of battery for apps.

<!--more-->

### How smart's a smart watch?

My expectations are low because I'll get more than I'm used to relying on. And since this is an open platform it's going to grow. Good integration means you can leave your phone at home, or at least that's the idea. It's up to the market at thsi point, but more on that later.

### Let's get a feel for it

The touchscreen functions like you'd expect on a modern phone without navigation keys. Swipe from the left to see notifications, quick settings from up top, widgets from the right side and app from the bottom. Also complicatios can be used right from the watch face.

Twisting the digital crown is something to get used to. That minimalist display with no border, though. And it handles nice. I barely notice the 40mm watch on my wrist in terms of weight and also the included wristband - which I might change for a different color. Overall slick and good quality.

Finally by way of the spidey gesture you can use NFC with Samsung Pay or Google Pay. With no phone on you and offline.

### I've got a strange déjà vu here

The settings exposed through the **Wear** app resemble those on a Samsung phone. Connections, display, even developer options are there[^1]. And you can pick the respective complications[^2]. And there's the apps, which you can simply get for the apps you have installed on your phone. With new apps you'll even see it suggest to install both at once. *Spotify* runs on the watch and supports locally saved playlists. *Komoot* shows planned tours and navigation directions. Or even *Hue Essentials* to change your lights.

[^1]: It's basically Android. So you keep hitting at the version number til you get a confirmation that it was enabled.
[^2]: Complications as in, the digital analogue of extra mechanisms like showing a date or nowadays your blood oressure.

Just one catch. It didn't take me too long to go through most of the apps and *PocketCasts* is one I am sorely missing. This means podcasts aren't automatically synched. Instead the only option is copying them manually. Supporting WearOS isn't worth it unless it gets more popular. And once Samsung loses its preferental treatment other companies will make WearOS devices and create a pool users want to choose from. Not to mention the successor already being in the works.

### I simply don't have the energy

Battery would be worth focussing on for the next iteration. I've not done detailed tests but it won't last a week in the best case. The battery isn't big and software can only be optimized so much.

Of course you can check that you only leave enabled what you need. For instance I'm quite happy to tap on the display than have it wake up on its own. I don't constantly look at the display. Still it's sobering that actually using all of the features of the watch seems to be too much for the battery. Maybe Samsung should've locked it down a bit to avoid that.

### Say Qi-se

Qi[^3] is like, just nice at this point. No special dongle that only works with this watch and nothing else. Any inductive pad, phone or powerbank will do. Incidentally it's hard-wired to USB-A which strikes me as a bit of an anachronism. It's not a huge bother since I use both one way or another, but a removeable cable would've been so much better. That said the magnetic grip is decent and you can't take that for granted.

[^3]: The `q` is pronounced like the `ci` in `ciao`, or the `ch` in `champion`.
